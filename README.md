# XiaoYou.Ump.Docs

#### 介绍
晓幽Ump文档

#### 软件架构
XiaoYou.Ump 
是ABP框架的一个精简版，只保留了abp框架的依赖注入，日志，事件总线内容。


#### 安装教程

1.  Nuget 搜索XiaoYou.Ump 可以查看目前所有的模块信息，通过vs的Nuget安装管理器安装即可

#### 模块信息

1.  XiaoYou.Ump 核心模块。
    - [模块系统](https://gitee.com/bxboy/XiaoYou.Ump.Docs/blob/master/模块系统.md)
    - [启动配置](https://gitee.com/bxboy/XiaoYou.Ump.Docs/blob/master/启动配置.md)
    - [依赖注入](https://gitee.com/bxboy/XiaoYou.Ump.Docs/blob/master/依赖注入.md)
    - [日志管理](https://gitee.com/bxboy/XiaoYou.Ump.Docs/blob/master/日志管理.md)
    - [领域事件](https://gitee.com/bxboy/XiaoYou.Ump.Docs/blob/master/领域事件.md)
2.  XiaoYou.Ump.Castle.Log4Net Log4Net日志模块。
3.  XiaoYou.Ump.Forms Winform模块，内置一个MainForm界面，可以在界面中添加tab扩展功能。
4.  XiaoYou.Ump.FreeSql 一个FreeSql模块，具体使用请参考FreeSql的功能。
5.  XiaoYou.Ump.TinHttpService 轻量级的Http模块。
6.  XiaoYou.Ump.LeidianSimulator 雷电模拟器操作模块。
7.  XiaoYou.Game.Data.Share 游戏数据公共模块。
8.  XiaoYou.Game.Data 游戏数据模块，内置IAccountManager 包含的账号的获取，更新，插入，删除，查询等操作。
9.  XiaoYou.Game.Central 游戏中控模块，账号导入，导出，获取等功能。
10. XiaoYou.Game.Client 游戏客户端模块，用于控制Player的生命周期。
11. XiaoYou.Game.Verification 游戏验证模块，开启后，必须验证通过后Client才能运行。
12. XiaoYou.Game.Player 用于运行脚本。

